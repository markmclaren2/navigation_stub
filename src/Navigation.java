public class Navigation {

    public static void backButtonPressed() {
        System.out.println("Back pressed");
    }

    public static void forwardButtonPressed() {
        System.out.println("Forward pressed");
    }

    public static void listItemSelected(int index) {
        MainWindow.setPanelColor(PanelColor.values()[index]);
    }

    public static void init() {
        MainWindow.setSelectedIndex(PanelColor.CYAN.ordinal());
        MainWindow.setPanelColor(PanelColor.CYAN);
        MainWindow.setComboIndex(0);
        MainWindow.setCheckboxTicked(true);
    }

    public static void typeSelected(int index) {
        System.out.println("Type droplist selected: " + index);
    }

    public static void checkboxClicked(boolean isTicked) {
        System.out.println("Checkbox clicked: " + isTicked);
    }
}
