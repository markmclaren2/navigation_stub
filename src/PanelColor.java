import org.eclipse.swt.SWT;

enum PanelColor {
    CYAN("Cyan", SWT.COLOR_CYAN), BLUE("Blue", SWT.COLOR_BLUE), YELLOW("Yellow", SWT.COLOR_YELLOW), BLACK("Black", SWT.COLOR_BLACK),
    DARK_BLUE("Dark Blue", SWT.COLOR_DARK_BLUE), RED("Red", SWT.COLOR_RED);

    public final int swtColor;
    public final String name;

    PanelColor(String name, int swtColor) {
        this.name = name;
        this.swtColor = swtColor;
    }
}
