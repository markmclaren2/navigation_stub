import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.internal.DPIUtil;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

public class MainWindow {

    private static List left;

    static SelectionAdapter listListener = new SelectionAdapter() {
        @Override
        public void widgetSelected(SelectionEvent e) {
            int selectionIndex = left.getSelectionIndex();
            if (selectionIndex != -1) {
                Navigation.listItemSelected(selectionIndex);
            }
        }
    };
    private static Composite mainPanel;
    private static SelectionAdapter comboListener = new SelectionAdapter() {
        @Override
        public void widgetSelected(SelectionEvent e) {
            Navigation.typeSelected(typeCombo.getSelectionIndex());
        }
    };
    private static Combo typeCombo;
    private static SelectionListener checkboxListener = new SelectionAdapter() {
        @Override
        public void widgetSelected(SelectionEvent e) {
            Navigation.checkboxClicked(checkbox.getSelection());
        }
    };
    private static Button checkbox;

    private static ToolItem backToolItem;
    private static ToolItem forwardToolItem;


    public static void main(String[] args) {
        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setText("Navigation Example");

        shell.setLayout(new GridLayout(2, false));

        ToolBar top = new ToolBar(shell, SWT.FLAT | SWT.RIGHT);
        backToolItem = createToolItem(display, top, "back_64.png", "Back", () -> Navigation.backButtonPressed());
        forwardToolItem = createToolItem(display, top, "forward_64.png", "Forward", () -> Navigation.forwardButtonPressed());


        GridData topData = new GridData();
        topData.horizontalSpan = 2;
        top.setLayoutData(topData);

        left = new List(shell, SWT.NONE);

        for (PanelColor p :
                PanelColor.values()) {
            left.add(p.name);
        }

        GridData leftLayoutData = new GridData(SWT.LEFT, SWT.FILL, false, true
        );
        leftLayoutData.widthHint = left.computeSize(SWT.DEFAULT, SWT.DEFAULT).x * 2;
        leftLayoutData.verticalSpan = 2;
        left.setLayoutData(leftLayoutData);


        Composite filterPanel = new Composite(shell, SWT.NONE);
        filterPanel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        filterPanel.setLayout(new GridLayout(3, false));
        filterPanel.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
        new Label(filterPanel, SWT.LEFT).setText("Show:");
        typeCombo = new Combo(filterPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
        typeCombo.add("Type A");
        typeCombo.add("Type B");
        typeCombo.add("Type C");
        typeCombo.addSelectionListener(comboListener);
        checkbox = new Button(filterPanel, SWT.CHECK);
        checkbox.setText("Include deleted");
        checkbox.addSelectionListener(checkboxListener);

        mainPanel = new Composite(shell, SWT.NONE);
        mainPanel.setLayoutData(new GridData(GridData.FILL_BOTH));

        left.addSelectionListener(listListener);
        Navigation.init();

        shell.open();
        left.forceFocus();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) display.sleep();
        }
        display.dispose();
    }

    private static ToolItem createToolItem(Display display, ToolBar top, String filename, String label, Runnable r) {
        ToolItem toolItem = new ToolItem(top, SWT.PUSH);
        toolItem.setText(label);
        toolItem.setImage(new Image(display, createImgDataProvider(display, filename)));
        toolItem.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                r.run();
            }
        });
        return toolItem;
    }

    static DPIUtil.AutoScaleImageDataProvider createImgDataProvider(Display d, String filename) {
        return new DPIUtil.AutoScaleImageDataProvider(d,
                new ImageData(MainWindow.class.getResourceAsStream(filename)), 200);
    }

    public static void setPanelColor(PanelColor panelColor) {
        mainPanel.setBackground(mainPanel.getDisplay().getSystemColor(panelColor.swtColor));
    }

    public static void setSelectedIndex(int index) {
        left.removeSelectionListener(listListener);
        left.setSelection(index);
        left.redraw();
        left.addSelectionListener(listListener);
    }


    public static void setComboIndex(int index) {
        typeCombo.removeSelectionListener(comboListener);
        typeCombo.select(index);
        typeCombo.addSelectionListener(comboListener);

    }

    public static void setCheckboxTicked(boolean ticked) {
        checkbox.removeSelectionListener(checkboxListener);
        checkbox.setSelection(ticked);
        checkbox.addSelectionListener(checkboxListener);
    }

    public static void setForwardToolItemEnabled(boolean enabled) {
        forwardToolItem.setEnabled(enabled);
    }

    public static void setBackToolItemEnabled(boolean enabled) {
        backToolItem.setEnabled(enabled);
    }
}
